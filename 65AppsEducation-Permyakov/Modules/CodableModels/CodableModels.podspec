Pod::Spec.new do |spec|
  spec.name         = "CodableModels"
  spec.version      = "0.0.1"
  spec.summary      = "CodableModels"
  spec.license      = { :type => "BSD" }
  spec.homepage     = "https://65apps.com"
  spec.author       = { "Anton Permyakov" => "krikzor@yandex.ru" }
  spec.platform     = :ios, "14.0"
  spec.swift_version = "5.0"
  spec.source       = { :path => "." }
  spec.source_files = "CodableModels/**/*.{h,m,swift,xib,storyboard}"
end
