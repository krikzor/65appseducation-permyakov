//
//  MarvelCharacterModel.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 29.11.2021.
//

import Foundation

public struct MarvelCharacterModel: Codable {
    public let id: Int?
    public let name: String?
    public let description: String?
    public let thumbnail: MarvelCharacterThumbnail?

}

public struct MarvelCharacterThumbnail: Codable {
    public let path: String?
    public let extensionOfImage: String?
    public var fullPath: String {
        if let path = path, let extensionOfImage = extensionOfImage {
            return path + "/standard_fantastic." +  extensionOfImage
        }
        return ""
    }

    private enum CodingKeys: String, CodingKey {
        case path
        case extensionOfImage = "extension"
    }

}
