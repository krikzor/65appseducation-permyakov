//
//  MD5Hash.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 29.11.2021.
//

import Foundation
import CryptoKit

public final class MD5Hash {
    public func MD5(string: String) -> String {
        let digest = Insecure.MD5.hash(data: string.data(using: .utf8) ?? Data())

        return digest.map {
            String(format: "%02hhx", $0)
        }.joined()
    }
}
