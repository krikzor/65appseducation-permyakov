//
//  NetworkService.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 29.11.2021.
//
import Foundation

public final class NetworkService {
    public init () { }

    let md5Hash = MD5Hash()

    public func networkRequest(fetch list: String, completion: @escaping (Result<Any, Error>) -> Void) {
        guard var url = URLComponents(string: Config.baseUrl + list) else { return }

        url.queryItems = [URLQueryItem(name: "apikey", value: Config.publicApiKey),
                          URLQueryItem(name: "hash", value: getHash()),
                          URLQueryItem(name: "ts", value: String(Int(NSDate().timeIntervalSince1970)))]

        var request: URLRequest = URLRequest(url: url.url!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        URLSession.shared.dataTask(with: request) { data, _, error in
            if let data = data, error == nil {
                completion(.success(data))
            } else if error != nil {
                completion(.failure(error!))
            }
        }
        .resume()
    }

    private func getHash() -> String {
        let timeStampString = String(Int(NSDate().timeIntervalSince1970))
        let concatedString = timeStampString + Config.privateApiKey + Config.publicApiKey
        return MD5Hash().MD5(string: concatedString)
    }
}
