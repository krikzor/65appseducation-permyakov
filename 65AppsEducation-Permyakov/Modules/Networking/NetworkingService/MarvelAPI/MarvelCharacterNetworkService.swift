//
//  MarvelCharacterNetworkService.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 29.11.2021.
//

import Foundation
import CodableModels

public final class MarvelCharacterNetworkService {
    let networkService = NetworkService()
    private let character = "characters"
    private let decoder = JSONDecoder()
    private let failureString = "Something went wrong, please try again later :("

    public init () {}

    public func getHeroesList(
        completion: @escaping ([MarvelCharacterModel]) -> Void,
        failureCompletion: @escaping (String) -> Void) {
        networkService.networkRequest(fetch: character) { result in
            switch result {
            case let .success(heroes):
                guard let data = heroes as? Data,
                      let marvelHeroes = self.parseToModel(data: data)
                else { return failureCompletion(self.failureString) }
                completion(marvelHeroes)
            case let .failure(error):
                failureCompletion(error.localizedDescription)
            }
        }
    }

    public func getCurrentHero(id: String,
                               completion: @escaping ([MarvelCharacterModel]) -> Void,
                               failureCompletion: @escaping (String) -> Void) {
        networkService.networkRequest(fetch: character + "/\(id)") { result in
            switch result {
            case .failure(let error): failureCompletion(error.localizedDescription)
            case .success(let hero) :
                guard let data = hero as? Data,
                      let heroModel = self.parseToModel(data: data) else {return failureCompletion(self.failureString)}
                completion(heroModel)
            }
        }
    }

    private func parseToModel(data: Data) -> [MarvelCharacterModel]? {
        guard
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
            let jsonData = json["data"] as? [String: Any],
            let jsonResults = jsonData["results"],
            let myData = try? JSONSerialization.data(withJSONObject: jsonResults, options: []),
            let marvelHeroes = try? self.decoder.decode([MarvelCharacterModel].self, from: myData) else {return nil }
        return marvelHeroes
    }

}
