//
//  AuthService.swift
//  Pods
//
//  Created by Antony on 08.01.2022.
//

import FirebaseAuth
import Foundation

public final class AuthService {
    public init() { }

    public func createUser(email: String,
                           password: String,
                           completionHandler: @escaping (Result<User, Error>) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { result, error in
            if let user = result?.user, error == nil {
                completionHandler(.success(user))
            } else {
                completionHandler(.failure(error!))
            }
        }
    }

    public func signIn(email: String, password: String,
                       completionHandler: @escaping (Result<User, Error>) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let user = result?.user, error == nil {
                completionHandler(.success(user))
            } else {
                completionHandler(.failure(error!))
            }
        }
    }

    public func signOut() -> Error? {
        do {
            try Auth.auth().signOut()
        } catch let error {
            return error
        }
        return nil
    }
}
