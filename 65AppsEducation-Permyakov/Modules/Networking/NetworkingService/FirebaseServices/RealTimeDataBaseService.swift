//
//  RealTimeDataBaseService.swift
//  Networking
//
//  Created by Antony on 09.01.2022.
//

import CodableModels
import FirebaseDatabase
import Foundation

public final class RealTimeDataBaseService {
    public init() { }

    var dataBase: DatabaseReference = Database
        .database(url: "https://education-permyakov-default-rtdb.europe-west1.firebasedatabase.app")
        .reference()

    public func addHeroToBase(character: MarvelCharacterModel,
                              userID: String) {
        guard let data = try? JSONEncoder().encode(character),
              let dataToJSON = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
              as? [String: Any],
              let characterId = character.id
        else { return }
        dataBase.child("user" + userID).child("results").updateChildValues(["\(characterId)": dataToJSON])
    }

    public func loadFromDataBase(userID: String, completion: @escaping (([MarvelCharacterModel]?) -> Void)) {
        dataBase.child("user" + userID).observeSingleEvent(of: .value) { snapshot in
            guard let value = snapshot.value as? [String: Any] else { return completion([]) }
            guard let data = try? JSONSerialization.data(withJSONObject: value, options: []),
                  let heroesInBase = try? JSONDecoder().decode([String: [String: MarvelCharacterModel]].self,
                                                               from: data)
            else {
                return completion([])
            }
            var modelArray = [MarvelCharacterModel]()
            for hero in heroesInBase.values {
                for item in hero {
                    modelArray.append(item.value)
                }
            }
            completion(modelArray)
        }
    }

    public func deleteFromDataBase(userID: String,
                                   characterList: [MarvelCharacterModel],
                                   removeHero: MarvelCharacterModel,
                                   completion: @escaping ((Bool) -> Void)) {
        guard let characterId = removeHero.id else { return}
        let url: String = "https://education-permyakov-default-rtdb.europe-west1.firebasedatabase.app"
        for item in characterList where item.id == removeHero.id {
            Database.database(url: url)
                .reference()
                .child("user" + userID)
                .child("results")
                .child(String(characterId)).removeValue()
            completion(true)
            break
        }
    }

}
