Pod::Spec.new do |spec|
  spec.name         = "Networking"
  spec.version      = "0.0.1"
  spec.summary      = "Networking"
  spec.license      = { :type => "BSD" }
  spec.homepage     = "https://65apps.com"
  spec.author       = { "Anton Permyakov" => "krikzor@yandex.ru" }
  spec.platform     = :ios, "14.0"
  spec.swift_version = "5.0"
  spec.source       = { :path => "." }
  spec.source_files = "NetworkingService/**/*.{h,m,swift,xib,storyboard}"
  spec.dependency  "CodableModels"
  spec.dependency "Firebase"
  spec.static_framework = true
end
