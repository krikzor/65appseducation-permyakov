//
//  CreateAccountViewController.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 17.10.2021.
//

import UIKit

final class CreateAccountViewController: UIViewController, CreateAccountViewable {

    // MARK: - IBOutlets

    @IBOutlet var createAccButton: UIButton!
    @IBOutlet var haveAccButton: UIButton!
    @IBOutlet var agreeLabel: UILabel!
    @IBOutlet var confirmPasswordTextField: UITextField!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var scrollViewHeight: NSLayoutConstraint!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var nameTextField: UITextField!

    // MARK: - Properties

    private var presenter: CreateAccountPresenterProtocol!
    private var notificationCenter = NotificationCenter.default
    private let willShowName = UIResponder.keyboardWillShowNotification
    private let willHideName = UIResponder.keyboardWillHideNotification

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = CreateAccountPresenter(view: self)
        colorize()
        setupTextFields()
        hideKeyboardWhenTappedAround()
        title = "Create account"
        navigationItem.backButtonTitle = ""
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShow), name: willShowName, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillHide), name: willHideName, object: nil)
        createAccButton.addTarget(self, action: #selector(createButtonDidTap), for: .touchUpInside)
        haveAccButton.addTarget(self, action: #selector(alreadyHaveAccDidTap), for: .touchUpInside)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let navHeight = navigationController?.navigationBar.frame.height ?? 0
        scrollViewHeight.constant = view.frame.height - navHeight
    }

    // MARK: - Supported methods

    func colorize() {
        scrollView.backgroundColor = .clear
        scrollView.bounces = false
        scrollView.showsVerticalScrollIndicator = false
        view.backgroundColor = Colors.backGroundColor
        // MARK: Попытки уместить длину строки для SwiftLint тщетны
        confirmPasswordTextField.attributedPlaceholder = .init(string: "Confirm Password",
                                                        attributes: [.foregroundColor: Colors.placeHolder])
        passwordTextField.attributedPlaceholder = .init(string: "Password",
                                                                     attributes: [.foregroundColor: Colors.placeHolder])
        emailTextField.attributedPlaceholder = .init(string: "Email",
                                                                  attributes: [.foregroundColor: Colors.placeHolder])
        nameTextField.attributedPlaceholder = .init(string: "Name",
                                                                 attributes: [.foregroundColor: Colors.placeHolder])
        confirmPasswordTextField.textColor = Colors.placeHolder
        passwordTextField.textColor = Colors.placeHolder
        emailTextField.textColor = Colors.placeHolder
        nameTextField.textColor = Colors.placeHolder
    }

    func setupTextFields() {
        confirmPasswordTextField.setLeftPaddingPoints(20)
        passwordTextField.setLeftPaddingPoints(20)
        emailTextField.setLeftPaddingPoints(20)
        nameTextField.setLeftPaddingPoints(20)

        confirmPasswordTextField.font = UIFont(name: "OpenSans-SemiBold", size: 14)
        passwordTextField.font = UIFont(name: "OpenSans-SemiBold", size: 14)
        emailTextField.font = UIFont(name: "OpenSans-SemiBold", size: 14)
        nameTextField.font = UIFont(name: "OpenSans-SemiBold", size: 14)

        createAccButton.titleLabel?.font = UIFont(name: "OpenSans-Bold", size: 16)
        agreeLabel.font = UIFont(name: "OpenSans-SemiBold", size: 14)
        haveAccButton.titleLabel?.font = UIFont(name: "OpenSans-Bold", size: 16)
        confirmPasswordTextField.isSecureTextEntry = true
        passwordTextField.isSecureTextEntry = true
    }

    // MARK: - Actions

    func createTap() {
        navigationController?.popToRootViewController(animated: true)
    }

    func alrdyHaveAccTap() {
        navigationController?.popToRootViewController(animated: true)
    }

    func showError(_ withMessage: String) {
        let alert = UIAlertController(title: nil, message: withMessage, preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "Ok", style: .destructive, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }

    @objc
    func createButtonDidTap() {
        guard let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
              let password = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
              let passwordConfrim = confirmPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
              let name = nameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        else {return showError(ErrorConstants.emptyField)}
        guard password.trimmingCharacters(in: .whitespacesAndNewlines)
                == passwordConfrim.trimmingCharacters(in: .whitespacesAndNewlines)
        else { return showError(ErrorConstants.notEqualPasswords)}
        presenter.createAccount(email: email, password: password)
    }

    @objc
    func alreadyHaveAccDidTap() {
        presenter.popToRoot()
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        if  var keyboardFrame: CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset: UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height + 20
            scrollView.contentInset = contentInset
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInset: UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
}
