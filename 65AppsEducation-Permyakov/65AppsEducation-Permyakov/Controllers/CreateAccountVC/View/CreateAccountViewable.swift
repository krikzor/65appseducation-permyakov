//
//  CreateAccountViewable.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 19.11.2021.
//

import Foundation

protocol CreateAccountViewable: AnyObject {
    func createTap()
    func alrdyHaveAccTap()
    func showError(_ withMessage: String)
}
