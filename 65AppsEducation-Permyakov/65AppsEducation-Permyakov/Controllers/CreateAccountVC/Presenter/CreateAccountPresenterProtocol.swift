//
//  CreateAccountPresenterProtocol.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 19.11.2021.
//

import Foundation

protocol CreateAccountPresenterProtocol {
    var view: CreateAccountViewable {get set}
    func popToRoot()
    func createAccount(email: String, password: String)
}
