//
//  CreateAccountPresenter.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 19.11.2021.
//

import Foundation
import Networking
import FirebaseAuth

final class CreateAccountPresenter: CreateAccountPresenterProtocol {

    unowned var view: CreateAccountViewable

    init(view: CreateAccountViewable) {
        self.view = view
    }

    func popToRoot() {
        view.createTap()
    }

    func createAccount(email: String, password: String) {
        if let emailError = email.isValidEmail() {
            view.showError(emailError)
            return
        } else if let passwordError = password.isValidPassword() {
            view.showError(passwordError)
            return
        }

        AuthService().createUser(email: email, password: password) { [weak self] result in
            switch result {
            case .failure(let error):
                self?.view.showError(error.localizedDescription)
            case .success(let user):
                self?.popToRoot()
            }
        }
    }
}
