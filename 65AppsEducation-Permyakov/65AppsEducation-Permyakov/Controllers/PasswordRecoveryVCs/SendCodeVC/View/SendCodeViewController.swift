//
//  SendCodeViewController.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 17.10.2021.
//

import UIKit

class SendCodeViewController: UIViewController, SendCodeViewable {
    // MARK: - UI elements

    private var emailTextField: UITextField = {
        let textField: UITextField = .init()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.textColor = Colors.placeHolder
        textField.font = UIFont(name: "OpenSans-SemiBold", size: 14)
        textField.setLeftPaddingPoints(20)
        textField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                             attributes: [.foregroundColor: Colors.placeHolder])
        return textField
    }()

    private var sendCodeButton: UIButton = {
        let button: UIButton = .init()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .systemRed
        button.setTitle("SEND CODE", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "OpenSans-Bold", size: 16)
        return button
    }()

    private var presenter: SendCodePresenterProtocol!

    // MARK: - View lifecycle

    override func viewDidLoad() {
        presenter = SendCodePresenter(view: self)
        super.viewDidLoad()
        view.backgroundColor = Colors.backGroundColor
        title = "Password recovery"
        navigationItem.backButtonTitle = ""
        setupViews()
        hideKeyboardWhenTappedAround()
        sendCodeButton.addTarget(self, action: #selector(didTapSendCode), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }

    // MARK: - Supported methods

    func setupViews() {
        view.addSubview(emailTextField)
        view.addSubview(sendCodeButton)
        NSLayoutConstraint.activate([
            emailTextField.heightAnchor.constraint(equalToConstant: 50),
            emailTextField.topAnchor.constraint(equalTo: view.topAnchor, constant: 250),
            emailTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 28),
            emailTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -27),

            sendCodeButton.heightAnchor.constraint(equalToConstant: 50),
            sendCodeButton.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 33),
            sendCodeButton.leadingAnchor.constraint(equalTo: emailTextField.leadingAnchor),
            sendCodeButton.trailingAnchor.constraint(equalTo: emailTextField.trailingAnchor)
        ])
    }

    // MARK: - Actions

    @objc
    func didTapSendCode() {
        presenter.presentSendCodeVC()
    }

    func tapSendCode() {
        navigationController?.pushViewController(SaveNewPassViewController(), animated: true)
    }
}
