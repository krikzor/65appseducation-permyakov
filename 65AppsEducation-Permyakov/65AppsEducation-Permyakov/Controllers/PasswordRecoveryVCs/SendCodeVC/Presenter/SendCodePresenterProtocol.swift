//
//  SendCodePresenterProtocol.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 19.11.2021.
//

import Foundation

protocol SendCodePresenterProtocol {
    var view: SendCodeViewable {get set}
    func presentSendCodeVC()
}
