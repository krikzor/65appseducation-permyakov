//
//  SendCodePresenter.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 19.11.2021.
//

import Foundation

final class SendCodePresenter: SendCodePresenterProtocol {
    unowned var view: SendCodeViewable

    init(view: SendCodeViewable) {
        self.view = view
    }

    func presentSendCodeVC() {
        view.tapSendCode()
    }
}
