//
//  SaveNewPassPresenter.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 19.11.2021.
//

import Foundation

final class SaveNewPassPresenter: SaveNewPassPresenterProtocol {
    unowned var view: SaveNewPassViewable

    init(view: SaveNewPassViewable) {
        self.view = view
    }

    func popToRoot() {
        view.saveTap()
    }
}
