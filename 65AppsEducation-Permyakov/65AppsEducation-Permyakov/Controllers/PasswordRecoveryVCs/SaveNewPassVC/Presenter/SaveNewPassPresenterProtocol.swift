//
//  SaveNewPassPresenterProtocol.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 19.11.2021.
//

import Foundation

protocol SaveNewPassPresenterProtocol {
    var view: SaveNewPassViewable { get set}
    func popToRoot()
}
