//
//  SaveNewPassViewController.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 17.10.2021.
//

import UIKit

final class SaveNewPassViewController: UIViewController, SaveNewPassViewable {
    // MARK: - UI elements

    private var passwordTextField: UITextField = {
        let textField: UITextField = .init()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.textColor = Colors.placeHolder
        textField.font = UIFont(name: "OpenSans-SemiBold", size: 14)
        textField.isSecureTextEntry = true
        textField.setLeftPaddingPoints(20)
        textField.attributedPlaceholder = .init(string: "Password", attributes: [.foregroundColor: Colors.placeHolder])
        return textField
    }()

    private var confirmPasswordTextField: UITextField = {
        let textField: UITextField = .init()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = Colors.placeHolder
        textField.backgroundColor = .white
        textField.font = UIFont(name: "OpenSans-SemiBold", size: 14)
        textField.isSecureTextEntry = true
        textField.font = .systemFont(ofSize: 14)
        textField.setLeftPaddingPoints(20)
        textField.attributedPlaceholder = NSAttributedString(string: "Confirm password",
                                                             attributes: [.foregroundColor: Colors.placeHolder])
        return textField
    }()

    private var savePassButton: UIButton = {
        let button: UIButton = .init()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .systemRed
        button.setTitle("SAVE NEW PASSWORD", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "OpenSans-Bold", size: 16)
        return button
    }()

    private var presenter: SaveNewPassPresenterProtocol!

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = SaveNewPassPresenter(view: self)
        view.backgroundColor = Colors.backGroundColor
        title = "Password recovery"
        navigationItem.backButtonTitle = ""
        hideKeyboardWhenTappedAround()
        setupViews()
        savePassButton.addTarget(self, action: #selector(saveButtonTapped), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }

    // MARK: - Supported methods

    func setupViews() {
        view.addSubview(passwordTextField)
        view.addSubview(confirmPasswordTextField)
        view.addSubview(savePassButton)
        NSLayoutConstraint.activate([
            passwordTextField.heightAnchor.constraint(equalToConstant: 50),
            passwordTextField.topAnchor.constraint(equalTo: view.topAnchor, constant: 250),
            passwordTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 28),
            passwordTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -27),

            confirmPasswordTextField.heightAnchor.constraint(equalToConstant: 50),
            confirmPasswordTextField.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 10),
            confirmPasswordTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 28),
            confirmPasswordTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -27),

            savePassButton.heightAnchor.constraint(equalToConstant: 50),
            savePassButton.topAnchor.constraint(equalTo: confirmPasswordTextField.bottomAnchor, constant: 20),
            savePassButton.leadingAnchor.constraint(equalTo: confirmPasswordTextField.leadingAnchor),
            savePassButton.trailingAnchor.constraint(equalTo: confirmPasswordTextField.trailingAnchor)
        ])
    }

    // MARK: - Actions

    func saveTap() {
        navigationController?.popToRootViewController(animated: true)
    }

    @objc func saveButtonTapped() {
        presenter.popToRoot()
    }
}
