//
//  SaveNewPassViewable.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 19.11.2021.
//

import Foundation

protocol SaveNewPassViewable: AnyObject {
    func saveTap()
}
