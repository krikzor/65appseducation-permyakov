//
//  MarvelCharactersListPresenter.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 29.11.2021.
//

import Foundation
import Networking
import CodableModels
import FirebaseAuth

final class MarvelCharactersListPresenter: MarvelCharactersListPresenterProtocol {

    var characters: [MarvelCharacterModel] = []
    unowned var view: MarvelCharactersListViewable
    private var charactersNetworkService = MarvelCharacterNetworkService()

    init(view: MarvelCharactersListViewable) {
        self.view = view
    }

    func getCharactersList() {
        charactersNetworkService.getHeroesList { [weak self] arrayOfHeroes in
            DispatchQueue.main.async {
                self?.characters = arrayOfHeroes
                self?.view.showCharactersList()
            }
        } failureCompletion: {  [weak self] (error) in
            DispatchQueue.main.async {
                self?.view.showError(error)
            }
        }
    }

    func signOutUser() {
        if let error = AuthService().signOut() {
            view.showError(error.localizedDescription)
        } else {
            view.successSignOut()
        }
    }
}
