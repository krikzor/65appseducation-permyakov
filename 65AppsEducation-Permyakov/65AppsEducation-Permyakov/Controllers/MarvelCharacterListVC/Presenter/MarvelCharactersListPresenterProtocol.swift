//
//  MarvelCharactersListPresenterProtocol.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 29.11.2021.
//

import Foundation
import CodableModels

protocol MarvelCharactersListPresenterProtocol {
    var view: MarvelCharactersListViewable {get set}
    var characters: [MarvelCharacterModel] {get set}
    func getCharactersList()
    func signOutUser()
}
