//
//  MarvelCharactersListViewable.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 29.11.2021.
//

import Foundation

protocol MarvelCharactersListViewable: AnyObject {
    func showCharactersList()
    func showError(_ message: String)
    func successSignOut()
}
