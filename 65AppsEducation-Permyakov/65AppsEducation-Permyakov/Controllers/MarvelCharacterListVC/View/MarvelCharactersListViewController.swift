//
//  MarvelCharactersListViewController.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 29.11.2021.
//

import UIKit

final class MarvelCharactersListViewController: UIViewController, MarvelCharactersListViewable {

    private var presenter: MarvelCharactersListPresenterProtocol!
    private var tableView: UITableView!
    private var activityIndicator: UIActivityIndicatorView!
    private var userID: String

    var signOutCallBack: (() -> Void)?
    init(userID: String) {
        self.userID = userID
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        addTableView()
        presenter = MarvelCharactersListPresenter(view: self)
        showActivityIndicator()
        title = "Marvel Characters List"
        addSignOutButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.setHidesBackButton(true, animated: false)
        presenter.getCharactersList()
    }

    private func addTableView() {
        tableView = .init(frame:
            CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 50),
            style: .plain)
        tableView.bounces = false
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 120
        tableView.backgroundColor = Colors.backGroundColor
        view.addSubview(tableView)
        tableView.register(UINib(nibName: "MarvelCharactersListTableViewCell", bundle: .main),
                           forCellReuseIdentifier: "Cell")
    }

    private func addSignOutButton() {
        let signOutButton = UIBarButtonItem(title: "Sign out",
                                            style: .plain,
                                            target: self,
                                            action: #selector(signOutAction))
        navigationItem.rightBarButtonItem = signOutButton

    }

    private func showActivityIndicator() {
        activityIndicator = .init(style: .medium)
        activityIndicator.center = view.center
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }

    private func showFailureAlert(_ message: String) {
        let failureAlert: UIAlertController = .init(title: "", message: message, preferredStyle: .alert)
        failureAlert.addAction(.init(title: "Ok", style: .default, handler: nil))
        present(failureAlert, animated: true, completion: nil)
    }

    func showCharactersList() {
        tableView.reloadData()
        activityIndicator.stopAnimating()
    }

    func showError(_ message: String) {
        showFailureAlert(message)
        activityIndicator.stopAnimating()
    }

    func successSignOut() {
        signOutCallBack?()
    }

    @objc private func signOutAction() {
        presenter.signOutUser()
    }

}

extension MarvelCharactersListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.characters.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "Cell",
            for: indexPath)
            as? MarvelCharactersListTableViewCell else { return UITableViewCell() }
        cell.initCell(character: presenter.characters[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentHero = presenter.characters[indexPath.row]
        guard let heroId = currentHero.id else { return }

        let detailVC = MarvelCharacterDetailViewController(heroId: heroId, userID: userID)
        navigationController?.pushViewController(detailVC, animated: true)
    }
}
