//
//  MarvelCharactersListTableViewCell.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 29.11.2021.
//

import UIKit
import Kingfisher
import CodableModels

final class MarvelCharactersListTableViewCell: UITableViewCell {
    @IBOutlet weak var characterImage: UIImageView!
    @IBOutlet weak var characterNameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func initCell(character: MarvelCharacterModel) {
        selectionStyle = .none
        characterNameLabel.text = character.name
        if let imgUrl = URL(string: character.thumbnail?.fullPath ?? "") {
            characterImage.kf.setImage(with: imgUrl)
        }
        characterImage.layer.cornerRadius = 50
        characterImage.layer.masksToBounds = true
        characterImage.layer.borderWidth = 1
        characterImage.layer.borderColor = UIColor.black.cgColor
        backgroundColor = Colors.backGroundColor
    }
}
