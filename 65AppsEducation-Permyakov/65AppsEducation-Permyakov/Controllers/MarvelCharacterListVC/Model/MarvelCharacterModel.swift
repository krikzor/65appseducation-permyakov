//
//  MarvelCharacterModel.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 29.11.2021.
//

import Foundation

struct MarvelCharacterModel: Codable {
    let id: Int?
    let name: String?
    let description: String?
    let thumbnail: MarvelCharacterThumbnail?
}

struct MarvelCharacterThumbnail: Codable {
    let path: String?
    let extensionOfImage: String?
    var fullPath: String {
        if let path = path, let extensionOfImage = extensionOfImage {
            return path + "/portrait_incredible." +  extensionOfImage
        }
        return ""
    }

    private enum CodingKeys: String, CodingKey {
        case path
        case extensionOfImage = "extension"
    }

}
