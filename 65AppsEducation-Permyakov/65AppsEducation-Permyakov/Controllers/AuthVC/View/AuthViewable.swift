//
//  AuthViewable.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 19.11.2021.
//

import Foundation

protocol AuthViewable: AnyObject {
    func tappedSignIn(userID: String)
    func tappedForgotPass()
    func tappedCreateAcc()
    func showError(_ withMessage: String)
}
