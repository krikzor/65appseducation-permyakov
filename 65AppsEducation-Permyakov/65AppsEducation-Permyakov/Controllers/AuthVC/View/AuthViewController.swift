//
//  ViewController.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 09.10.2021.
//

import UIKit

final class AuthViewController: UIViewController, AuthViewable {

    // MARK: - IBOutlet's

    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var signInButton: UIButton!
    @IBOutlet var forgotPasswordButton: UIButton!
    @IBOutlet var registerButton: UIButton!
    @IBOutlet var appLabel: UILabel!

    // MARK: - Properties

    private var presenter: AuthPresenterProtocol!

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = AuthPresenter(view: self)
        colorize()
        setupViews()
        hideKeyboardWhenTappedAround()

        registerButton.addTarget(self, action: #selector(showCreateAcc), for: .touchUpInside)
        forgotPasswordButton.addTarget(self, action: #selector(showForgotPass), for: .touchUpInside)
        signInButton.addTarget(self, action: #selector(showSignIn), for: .touchUpInside)

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    // MARK: - Supported methods

    func colorize() {
        navigationController?.navigationBar.barTintColor = UIColor.white

        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                     attributes: [.foregroundColor: Colors.placeHolder])
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                  attributes: [.foregroundColor: Colors.placeHolder])
    }

    func setupViews() {
        appLabel.font = UIFont(name: "OpenSans-ExtraBold", size: 48)
        passwordTextField.font = UIFont(name: "OpenSans-SemiBold", size: 14)
        emailTextField.font = UIFont(name: "OpenSans-SemiBold", size: 14)
        passwordTextField.isSecureTextEntry = true
        passwordTextField.setLeftPaddingPoints(20)
        emailTextField.setLeftPaddingPoints(20)

        forgotPasswordButton.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 14)
        registerButton.titleLabel?.font = UIFont(name: "OpenSans-Bold", size: 16)
        signInButton.titleLabel?.font = UIFont(name: "OpenSans-Bold", size: 16)
    }

    // MARK: - Actions

    @objc
    func showCreateAcc() {
        presenter.presentCreateNewAcc()
    }

    @objc
    func showSignIn() {
        guard let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
              let password = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        else { return showError(ErrorConstants.emptyField)}
        presenter.signIn(email: email, password: password)
    }

    @objc
    func showForgotPass() {
        presenter.presentForgotPass()
    }

    func tappedSignIn(userID: String) {
        navigationController?.pushViewController(CustomTabBarController(userID: userID), animated: true)
    }

    func tappedForgotPass() {
        navigationController?.pushViewController(SendCodeViewController(), animated: true)
    }

    func tappedCreateAcc() {
        navigationController?.pushViewController(CreateAccountViewController(), animated: true)
    }

    func showError(_ withMessage: String) {
        let alert = UIAlertController(title: nil, message: withMessage, preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "Ok", style: .destructive, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
