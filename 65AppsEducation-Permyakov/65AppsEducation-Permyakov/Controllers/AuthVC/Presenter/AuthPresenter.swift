//
//  AuthPresenter.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 19.11.2021.
//

import Foundation
import Networking

final class AuthPresenter: AuthPresenterProtocol {
    unowned var view: AuthViewable

    init(view: AuthViewable) {
        self.view = view
    }

    func signIn(email: String, password: String) {
        if let emailError = email.isValidEmail() {
            view.showError(emailError)
            return
        } else if let passwordError = password.isValidPassword() {
            view.showError(passwordError)
            return
        }

        AuthService().signIn(email: email, password: password) { [weak self] (result) in
            switch result {
            case .failure(let error): self?.view.showError(error.localizedDescription)
            case .success(let user):
                self?.view.tappedSignIn(userID: user.uid)
            }
        }
    }

    func presentForgotPass() {
        view.tappedForgotPass()
    }

    func presentCreateNewAcc() {
        view.tappedCreateAcc()
    }
}
