//
//  AuthPresenterProtocol.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 19.11.2021.
//

import Foundation

protocol AuthPresenterProtocol {
    var view: AuthViewable {get set}
    func signIn(email: String, password: String)
    func presentForgotPass()
    func presentCreateNewAcc()
}
