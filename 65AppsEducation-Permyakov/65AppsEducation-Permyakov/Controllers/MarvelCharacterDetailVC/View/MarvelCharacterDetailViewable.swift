//
//  MarvelCharacterDetailViewable.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 09.01.2022.
//

import Foundation
import CodableModels

protocol MarvelCharacterDetailViewable: AnyObject {
    func toggleAddButton(_ bool: Bool)
    func renderHero(hero: MarvelCharacterModel)
    func showError(_ withMessage: String)
}
