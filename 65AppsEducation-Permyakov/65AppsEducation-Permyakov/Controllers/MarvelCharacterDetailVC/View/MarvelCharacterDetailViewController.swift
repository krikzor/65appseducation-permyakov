//
//  MarvelCharacterDetailViewController.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 08.01.2022.
//

import CodableModels
import FirebaseDatabase
import UIKit

final class MarvelCharacterDetailViewController: UIViewController, MarvelCharacterDetailViewable {

    @IBOutlet var heroImageView: UIImageView!
    @IBOutlet var heroNameLabel: UILabel!
    @IBOutlet var heroDescriptionLabel: UILabel!
    private var activityIndicator: UIActivityIndicatorView!

    private var userID: String
    private var heroId: Int
    private var isFavourite = false {
        didSet {
            guard navigationItem.rightBarButtonItems != nil  else {return}
            if isFavourite {
                navigationItem.rightBarButtonItems![0].isEnabled = false
                navigationItem.rightBarButtonItems![1].isEnabled = true
                navigationItem.rightBarButtonItems![1].tintColor = .systemBlue
            } else {
                navigationItem.rightBarButtonItems![1].tintColor = .clear
                navigationItem.rightBarButtonItems![1].isEnabled = false
                navigationItem.rightBarButtonItems![0].isEnabled = true
                navigationItem.rightBarButtonItems![0].tintColor = .systemBlue
            }
        }
    }

    private var presenter: MarvelCharacterDetailPresenterProtocol!

    init(heroId: Int, userID: String) {
        self.heroId = heroId
        self.userID = userID
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = MarvelCharacterDetailPresenter(view: self, userID: userID)
        setupNavBar()
        setupFonts()
        presenter.loadCurrentHero(id: heroId)
        showActivityIndicator()
        heroImageView.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(openDetailImage))
        heroImageView.addGestureRecognizer(gesture)
    }

    private func setupFonts() {
        heroNameLabel.font = .systemFont(ofSize: 25, weight: .medium)
        heroDescriptionLabel.font = .systemFont(ofSize: 16)
    }

    private func setupNavBar() {
        let backButton = UIBarButtonItem()
        backButton.title = ""
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton

        let addToFavourite = UIBarButtonItem(title: "Add",
                                             style: UIBarButtonItem.Style.plain,
                                             target: self,
                                             action: #selector(addToFavouriteTapped))
        let deleteButton = UIBarButtonItem(title: "Delete",
                                           style: .plain,
                                           target: self,
                                           action: #selector(deleteFromFavourite))
        deleteButton.tintColor = .clear
        navigationItem.rightBarButtonItems = [addToFavourite, deleteButton]
    }

    private func showHideSubviews (isHidden: Bool) {
        heroNameLabel.isHidden = isHidden
        heroDescriptionLabel.isHidden = isHidden
        heroImageView.isHidden = isHidden
    }

    private func showActivityIndicator() {
        showHideSubviews(isHidden: true)
        activityIndicator = .init(style: .medium)
        activityIndicator.center = heroDescriptionLabel.center
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }

    @objc private func addToFavouriteTapped() {
        presenter.didTapAddToFavourite()
    }

    @objc private func deleteFromFavourite() {
        presenter.deleteFromBase()
    }
    @objc private func openDetailImage() {
        let viewController = CharacterDetailImageViewController()
        viewController.currentHero = presenter.character
        navigationController?.pushViewController(viewController, animated: true)
    }

    func toggleAddButton(_ bool: Bool) {
        isFavourite = bool
    }
    func renderHero(hero: MarvelCharacterModel) {
        heroNameLabel.text = hero.name
        heroDescriptionLabel.text = hero.description
        if let imageUrl = URL(string: hero.thumbnail?.fullPath ?? "") {
            heroImageView.contentMode = .scaleAspectFill
            heroImageView.kf.setImage(with: imageUrl)
        }
        showHideSubviews(isHidden: false)
        activityIndicator.stopAnimating()
    }
    func showError(_ withMessage: String) {
        let alert = UIAlertController(title: nil, message: withMessage, preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "Ok", style: .destructive, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
