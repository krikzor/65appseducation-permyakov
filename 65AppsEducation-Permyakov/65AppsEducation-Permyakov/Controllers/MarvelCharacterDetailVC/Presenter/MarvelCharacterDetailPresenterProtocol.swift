//
//  MarvelCharacterDetailPresenterProtocol.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 09.01.2022.
//

import Foundation
import CodableModels
import FirebaseDatabase

protocol MarvelCharacterDetailPresenterProtocol {
    var view: MarvelCharacterDetailViewable {get set}
    var character: MarvelCharacterModel? { get set }
    func loadCurrentHero(id: Int)
    func didTapAddToFavourite()
    func checkHeroInBase()
    func deleteFromBase()
}
