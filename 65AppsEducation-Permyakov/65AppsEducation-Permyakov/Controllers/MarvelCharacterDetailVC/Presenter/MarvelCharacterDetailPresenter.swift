//
//  MarvelCharacterDetailPresenter.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 09.01.2022.
//

import CodableModels
import FirebaseDatabase
import Foundation
import Networking

final class MarvelCharacterDetailPresenter: MarvelCharacterDetailPresenterProtocol {
    private var userID: String
    var view: MarvelCharacterDetailViewable
    var character: MarvelCharacterModel? {
        didSet {
            checkHeroInBase()
        }
    }

    init(view: MarvelCharacterDetailViewable,
         userID: String) {
        self.view = view
        self.userID = userID
    }

    func loadCurrentHero(id: Int) {
        MarvelCharacterNetworkService().getCurrentHero(id: String(id)) { result in
            DispatchQueue.main.async {
                guard let hero = result.first else { return self.view.showError(ErrorConstants.commonError) }
                self.view.renderHero(hero: hero)
                self.character = hero
            }
        } failureCompletion: { error in
            DispatchQueue.main.async {
                self.view.showError(error)
            }
        }
    }

    func didTapAddToFavourite() {
        DispatchQueue.main.async {
            guard let character = self.character else { return }
            RealTimeDataBaseService().addHeroToBase(character: character, userID: self.userID)
        }
        view.toggleAddButton(true)
    }

    func checkHeroInBase() {
        guard let hero = character else { return view.showError(ErrorConstants.commonError) }
        RealTimeDataBaseService().loadFromDataBase(userID: userID) { heroes in
            guard let heroes = heroes else { return }
            self.view.toggleAddButton(heroes.contains(where: { $0.id == hero.id }))
        }
    }

    func deleteFromBase() {
        guard let hero = character else { return }
        RealTimeDataBaseService().loadFromDataBase(userID: userID) { result in
            guard let heroes = result else { return }
            RealTimeDataBaseService().deleteFromDataBase(userID: self.userID,
                                                         characterList: heroes,
                                                         removeHero: hero) { _ in
                self.view.toggleAddButton(false)
            }
        }
    }
}
