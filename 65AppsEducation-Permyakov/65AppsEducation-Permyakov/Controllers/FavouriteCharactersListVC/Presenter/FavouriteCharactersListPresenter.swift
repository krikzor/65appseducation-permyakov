//
//  FavouriteCharactersListPresenter.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 09.01.2022.
//

import CodableModels
import FirebaseDatabase
import Foundation
import Networking

final class FavouriteCharactersListPresenter: FavouriteCharactersListPresenterProtocol {

    var view: FavouriteCharactersListViewable
    var characters: [MarvelCharacterModel] = []
    private var userID: String

    init(view: FavouriteCharactersListViewable,
         userID: String) {
        self.view = view
        self.userID = userID
    }

    func loadFavouriteCharacters() {
        DispatchQueue.main.async {
            RealTimeDataBaseService().loadFromDataBase(userID: self.userID) { (result) in
                guard let heroes = result else { return self.view.showError(ErrorConstants.commonError)}
                self.characters = heroes
                self.characters.isEmpty ? self.view.showEmptyList() : self.view.showCharactersList()
            }
        }
    }

}
