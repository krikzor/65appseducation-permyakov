//
//  FavouriteCharactersListPresenterProtocol.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 09.01.2022.
//

import Foundation
import CodableModels
import Firebase

protocol FavouriteCharactersListPresenterProtocol {
    var view: FavouriteCharactersListViewable {get set}
    var characters: [MarvelCharacterModel] {get set}
    func loadFavouriteCharacters()
}
