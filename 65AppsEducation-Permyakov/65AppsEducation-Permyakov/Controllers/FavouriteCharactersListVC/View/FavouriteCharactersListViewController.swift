//
//  FavouriteCharactersListViewController.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 09.01.2022.
//

import UIKit

final class FavouriteCharactersListViewController: UIViewController, FavouriteCharactersListViewable {

    // MARK: - Subviews

    private var tableView: UITableView!
    private var activityIndicator: UIActivityIndicatorView!
    private var emptyTitle: UILabel!

    // MARK: - Properties

    private var presenter: FavouriteCharactersListPresenterProtocol!
    private var userID: String

    // MARK: - View lifecycle

    init(userID: String) {
        self.userID = userID
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = FavouriteCharactersListPresenter(view: self, userID: userID)
        title = "Favourite Characters List"
        addTableView()
        addEmptyTitle()
        showActivityIndicator()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.loadFavouriteCharacters()
    }

    private func addTableView() {
        tableView = .init(frame:
                            CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 50),
                          style: .plain)
        tableView.bounces = false
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 120
        tableView.backgroundColor = Colors.backGroundColor
        view.addSubview(tableView)
        tableView.register(UINib(nibName: "MarvelCharactersListTableViewCell", bundle: .main),
                           forCellReuseIdentifier: "Cell")
    }

    private func showActivityIndicator() {
        activityIndicator = .init(style: .medium)
        activityIndicator.center = view.center
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    private func addEmptyTitle() {
        emptyTitle = .init(frame: CGRect(x: 0, y: 0, width: view.frame.width - 30, height: 250))
        emptyTitle.center = view.center
        emptyTitle.font = .systemFont(ofSize: 25, weight: .medium)
        emptyTitle.textColor = .systemBlue
        emptyTitle.numberOfLines = 0
        emptyTitle.textAlignment = .center
        emptyTitle.text = "Favorite heroes list is empty."
        emptyTitle.isHidden = true
        view.addSubview(emptyTitle)
    }

    func showCharactersList() {
        tableView.isHidden = false
        emptyTitle.isHidden = true
        tableView.reloadData()
        activityIndicator.stopAnimating()
    }

    func showError(_ message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "Ok", style: .destructive, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }

    func showEmptyList() {
        tableView.isHidden = true
        emptyTitle.isHidden = false
        activityIndicator.stopAnimating()
    }

}

extension FavouriteCharactersListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.characters.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
                withIdentifier: "Cell",
                for: indexPath)
                as? MarvelCharactersListTableViewCell else { return UITableViewCell() }
        cell.initCell(character: presenter.characters[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentHero = presenter.characters[indexPath.row]
        guard let heroId = currentHero.id else { return }
        let detailVC = MarvelCharacterDetailViewController(heroId: heroId, userID: userID)
        navigationController?.pushViewController(detailVC, animated: true)
    }
}
