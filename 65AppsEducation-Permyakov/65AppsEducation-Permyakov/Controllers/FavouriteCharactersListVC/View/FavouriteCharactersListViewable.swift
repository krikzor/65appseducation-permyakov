//
//  FavouriteCharactersListViewable.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 09.01.2022.
//

import Foundation

protocol FavouriteCharactersListViewable: AnyObject {
    func showCharactersList()
    func showError(_ message: String)
    func showEmptyList()
}
