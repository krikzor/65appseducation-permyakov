//
//  CustomTabBarController.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 09.01.2022.
//

import UIKit

final class CustomTabBarController: UITabBarController {

    private var userID: String

    init(userID: String) {
        self.userID = userID
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        let vc1 = UINavigationController(rootViewController: MarvelCharactersListViewController(userID: userID))
        let vc1TabItem = UITabBarItem(title: "Marvel Characters List",
                                      image: UIImage(systemName: "doc.plaintext"),
                                      tag: 0)
        vc1TabItem.selectedImage = UIImage(systemName: "doc.plaintext")
        vc1.tabBarItem = vc1TabItem

        if let viewController =  vc1.viewControllers[0] as? MarvelCharactersListViewController {
            viewController.signOutCallBack = { [weak self] in
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
        let vc2 = UINavigationController(rootViewController: FavouriteCharactersListViewController(userID: userID))
        let vc2TabItem = UITabBarItem(title: "Favourite Characters List",
                                      image: UIImage(systemName: "star.fill"),
                                      tag: 1)
        vc2TabItem.selectedImage = UIImage(systemName: "star.fill")
        vc2.tabBarItem = vc2TabItem
        viewControllers = [vc1, vc2]
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

}
