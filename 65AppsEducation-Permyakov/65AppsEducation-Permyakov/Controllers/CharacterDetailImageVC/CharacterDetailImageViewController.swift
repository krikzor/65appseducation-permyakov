//
//  CharacterDetailImageViewController.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 11.01.2022.
//

import Kingfisher
import UIKit
import CodableModels

final class CharacterDetailImageViewController: UIViewController, UIScrollViewDelegate {

    // MARK: - Subviews
    private var scrollV: UIScrollView!
    private var imageView: UIImageView!

    // MARK: - Properties
    var currentHero: MarvelCharacterModel?

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        let height: CGFloat = view.frame.height -
            (tabBarController?.tabBar.frame.height ?? 0) -
            (navigationController?.navigationBar.frame.height ?? 0)
        scrollV = UIScrollView()
        scrollV.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: height)
        scrollV.minimumZoomScale = 1
        scrollV.maximumZoomScale = 3
        scrollV.bounces = false
        scrollV.delegate = self
        view.addSubview(scrollV)

        imageView = UIImageView()
        imageView.frame = CGRect(x: 0, y: 0,
                                 width: scrollV.frame.width, height: height)
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleToFill
        guard let imageURL = URL(string: currentHero?.thumbnail?.fullPath ?? "" ) else { return }
        imageView.kf.setImage(with: imageURL)
        imageView.contentMode = .scaleAspectFit
        scrollV.addSubview(imageView)
        view.backgroundColor = Colors.backGroundColor
        imageView.backgroundColor = .clear
    }

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
