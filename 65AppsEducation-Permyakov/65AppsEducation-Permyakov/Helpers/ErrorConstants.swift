//
//  ErrorConstants.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 08.01.2022.
//

import Foundation

struct ErrorConstants {
    static var commonError = "Something was wrong"
    static var wrongEmail = "Wrong e-mail. Example: yourName@gmail.com"
    static var minPassword = "Min lenght of password 6 characters"
    static var notEqualPasswords = "Password mismatch"
    static var emptyField = "All fields must be filled"
}
