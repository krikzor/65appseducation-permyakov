//
//  String.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 08.01.2022.
//

import Foundation

extension String {
    func isValidEmail() -> String? {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        let bool =  emailPred.evaluate(with: self)
        if bool == false {
            return ErrorConstants.wrongEmail
        }
        return nil
    }
    func isValidPassword() -> String? {
        let finalText = self.trimmingCharacters(in: .whitespacesAndNewlines)
        if finalText.count < 6 {
            return ErrorConstants.minPassword
        }
        return nil
    }
}
