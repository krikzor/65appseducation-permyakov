//
//  Colors.swift
//  65AppsEducation-Permyakov
//
//  Created by Antony on 17.10.2021.
//

import Foundation
import UIKit

struct Colors {
    static let placeHolder = UIColor(red: 0.545, green: 0.6, blue: 0.624, alpha: 1)
    static let backGroundColor = UIColor(red: 0.898, green: 0.898, blue: 0.898, alpha: 1)
}
